from math import pi,sin

fname = "data_in.txt"


file = open(fname,"w")
fs = 100
T = 1/fs
L = 1000

#amplitudes aumentadas para gerar arquivo com numeros inteiros

f1 = 1.5
f2 = 2 * f1
a1 = 0.8 * 682
a2 = 0.7 * 682
ph1 = 0
ph2 = -0.8


for n in range(L):
    t = n*T
    yp1 = a1*sin(f1*pi*t+ph1)
    yp2 = a2*sin(f2*pi*t+ph2)
    y = yp1 + yp2

    file.write(f"{int(y)}\n")

file.close()



#data visualization below

# import pandas as pd
# from matplotlib import pyplot as plt

# sample = pd.read_csv(fname)
# plt.plot(sample)
# plt.show()