rom = open("rom.txt","w")
contador = 0
for c in range(0,1024,1):
	contador += 1
	if c == 0:
		bpm = 0
	else:
		bpm = int((1000//c)*6)
	if bpm > 255:
		bpm = 255
	if contador == 8:
		e = "\n"
		contador = 0
	else:
		e = ""
	print(f'"{bin(bpm)[2:].zfill(8)}",',end = e,file = rom)

rom.close()
