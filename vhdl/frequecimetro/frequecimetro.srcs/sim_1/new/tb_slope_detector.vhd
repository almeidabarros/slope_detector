library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use std.textio.all;
use ieee.std_logic_textio.all;


entity tb_slope_detector is
end tb_slope_detector;



architecture Behavioral of tb_slope_detector is

   	component slope_detector port ( 
data_in : in STD_LOGIC_VECTOR (10 downto 0);
clk : in STD_LOGIC;
rst : in STD_LOGIC;
slope : out STD_LOGIC
    );
   	end component;


	constant undefined : integer := 1;

	constant CLOCK_PERIOD_NS : time := 8333 us;

	constant NEW_DATA_CLK_COUNT : integer := 1;

	constant RESET_CLK_COUNT : integer := 10;


	file input_file : text is in "C:\Users\Vinicius\Documents\slope\vhdl\data\data_bin.txt";



	signal clock_count : integer := 0;

	signal error_count : integer := 0;


	type t_test_status is (
	  	TEST_STATUS_RESET,    
	   	TEST_STATUS_RUNNING,  
	   	TEST_STATUS_FINISHED, 
	   	TEST_STATUS_FAIL,     
	  	TEST_STATUS_OK        
	); 


	signal test_status : t_test_status := TEST_STATUS_RESET;

	

	signal s_data_in : STD_LOGIC_VECTOR (10 downto 0);
signal s_clk : STD_LOGIC :='0';
signal s_rst : STD_LOGIC;
signal s_slope : STD_LOGIC;

	begin
   
  	s_clk <= not s_clk after CLOCK_PERIOD_NS/2;
   
   	DUT: slope_detector
   	port map (
	data_in => s_data_in,
	clk => s_clk,
	rst => s_rst,
	slope => s_slope
   			);

	test : process( s_clk )
		variable v_line : line;
		variable v_counter : integer;
		variable v_data_in : STD_LOGIC_VECTOR (10 downto 0);



	begin
		if rising_edge( s_clk ) then
		 	case test_status is
				when TEST_STATUS_RESET => 
			   		if s_rst = '1' then
				  		v_counter := clock_count + 1;
				  
				  		if v_counter = RESET_CLK_COUNT then
					 		test_status <= TEST_STATUS_RUNNING;
					 		s_rst <= '0';
					 		clock_count <= 0;
				  		else
					 		clock_count <= v_counter;
				  		end if;

				  
--				 		if(

--                    	)
--				 		then
--							null;
--				  		else
--							error_count <= error_count + 1;
--				  		end if;
			   		else
					  	assert false report 
					  	LF & "###########################################" &
					  	LF & "Test s_rst = '1' for 10 clock cycles" &
					  	LF & "###########################################"
					  	severity note;

					  	s_rst <= '1'; 
					  	test_status <= TEST_STATUS_RESET;
					  	clock_count <= 0;
			   		end if;
   
				when TEST_STATUS_RUNNING =>
			   		v_counter := clock_count + 1;
			   			if v_counter = NEW_DATA_CLK_COUNT then	  
						  	if not endfile( input_file ) then
							 	readline( input_file, v_line );
			read(v_line,v_data_in);
			s_data_in <= v_data_in;
						  	end if;
						  	
				  			
			   			else
				  			clock_count <= v_counter;
			   			end if;
			   
						if endfile( input_file ) then
					 		test_status <= TEST_STATUS_FINISHED;
				  		else 
					 		null;
				  		end if;

--			   	if undefined = 1 then                
--				  	if(

--                    )
--				   	then
--					 	null;
--				  	else 
--					 	error_count <= error_count + 1;
--				  	end if;

			
			when TEST_STATUS_FINISHED =>
			   if error_count = 0 then
				  test_status <= TEST_STATUS_OK;
			   else
				  test_status <= TEST_STATUS_FAIL;
			   end if;
			
			when TEST_STATUS_OK =>
			   assert false report
			   LF & "###########################################" &
			   LF & "slope_detector is approved" &
			   LF & "###########################################"
			   severity note;

			   std.env.finish;
			
			when others =>
			   assert false report
			   LF & "###########################################" &
			   LF & "slope_detector is not approved" &
			   LF & "###########################################"
			   severity note;

			   std.env.finish;
		 end case;
	  end if;
   end process test;
end Behavioral;


