library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.frequencimetro_pkg.all;


entity tb_top_freq is
end tb_top_freq;



architecture Behavioral of tb_top_freq is

component top_freq port ( 
rst : in STD_LOGIC;
clk : in STD_LOGIC;
data_in : in STD_LOGIC_VECTOR (data_size -1 downto 0);
freq_out : out STD_LOGIC_VECTOR (freq_size -1 downto 0);
freq_inst : out STD_LOGIC_VECTOR (freq_size -1 downto 0);
freq_ready : out STD_LOGIC
    );
   	end component;


	constant undefined : integer := 1;

	constant CLOCK_PERIOD_NS : time := 10000 us;

	constant NEW_DATA_CLK_COUNT : integer := 25;

	constant RESET_CLK_COUNT : integer := 10;


	file input_file : text is in "C:\Users\Vinicius\Documents\slope\vhdl\data\data_bin.txt";

	signal clock_count : integer := 0;

	signal error_count : integer := 0;

	type t_test_status is (
	  	TEST_STATUS_RESET,    
	   	TEST_STATUS_RUNNING,  
	   	TEST_STATUS_FINISHED,    
	  	TEST_STATUS_OK        
	); 


	signal test_status : t_test_status := TEST_STATUS_RESET;

signal s_rst : STD_LOGIC;
signal s_clk : STD_LOGIC :='0';
signal s_data_in : STD_LOGIC_VECTOR (data_size -1 downto 0) := (others => '0');
signal s_freq_out : STD_LOGIC_VECTOR (freq_size -1 downto 0);
signal s_freq_inst : STD_LOGIC_VECTOR (freq_size -1 downto 0);
signal s_freq_ready : STD_LOGIC;

	begin
   
  	s_clk <= not s_clk after CLOCK_PERIOD_NS/2;
   
   	DUT: top_freq
   	port map (
	rst => s_rst,
	clk => s_clk,
	data_in => s_data_in,
	freq_out => s_freq_out,
	freq_inst => s_freq_inst,
	freq_ready => s_freq_ready
   			);
	test : process( s_clk )
		variable v_line : line;
		variable v_counter : integer;
		variable v_data_in : STD_LOGIC_VECTOR (data_size -1 downto 0);
		variable v_freq_out : STD_LOGIC_VECTOR (freq_size -1 downto 0);

	begin
		if rising_edge( s_clk ) then
		 	case test_status is
				when TEST_STATUS_RESET => 
			   		if s_rst = '1' then
				  		v_counter := clock_count + 1;
				  
				  		if v_counter = RESET_CLK_COUNT then
					 		test_status <= TEST_STATUS_RUNNING;
					 		s_rst <= '0';
					 		clock_count <= 0;
				  		else
					 		clock_count <= v_counter;
				  		end if;
	                       v_freq_out := (others => '0');
				  
	
			   		else
					  	assert false report 
					  	LF & "###########################################" &
					  	LF & "Test s_rst = '1' for 10 clock cycles" &
					  	LF & "###########################################"
					  	severity note;

					  	s_rst <= '1'; 
					  	test_status <= TEST_STATUS_RESET;
					  	clock_count <= 0;
			   		end if;
   
				when TEST_STATUS_RUNNING =>
			   		v_counter := clock_count + 1;
			   			if v_counter = NEW_DATA_CLK_COUNT then	  
						  	if not endfile( input_file ) then
							 	readline( input_file, v_line );
                                read(v_line,v_data_in);
                                s_data_in <= v_data_in;
						  	end if;


			   			else
				  			clock_count <= v_counter;
			   			end if;
			   
						if endfile( input_file ) then
					 		test_status <= TEST_STATUS_FINISHED;
				  		else 
					 		null;
				  		end if;

			
			when TEST_STATUS_FINISHED =>
			   std.env.finish;
			
			when others =>
			   std.env.finish;
		 end case;
	  end if;
   end process test;
end Behavioral;


