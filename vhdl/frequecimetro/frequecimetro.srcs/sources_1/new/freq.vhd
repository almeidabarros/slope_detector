library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.frequencimetro_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity freq is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           slope_in : in STD_LOGIC;
           tl_out : out STD_LOGIC;
           counter_out : out STD_LOGIC_VECTOR (counter_size - 1 downto 0); --counter_out_size = 10
           counter_ready : out STD_LOGIC);
end freq;

architecture Behavioral of freq is


signal counter : unsigned(counter_size -1 downto 0);      --counter_size = 10

type t_state is (init,counting,show,too_long);

signal state : t_state;

signal previous_slope : std_logic;

signal  count_peaks : unsigned(count_peaks_size -1 downto 0);     --count_peaks_size = 3
signal  count_wave : unsigned(count_wave_size - 1 downto 0);      --count_wave_size = 3
                                            
begin


 process(clk)
    begin 
    
    if(rising_edge(clk)) then 
        previous_slope <= slope_in;
    end if;
    
end process;

process(clk,rst) 
begin
    
    if rst = '1' then 
        counter <= (others => '0');
        state <= init;
        counter_out <= (others => '1');
        counter_ready <= '0';
        count_peaks <= (others => '0');
        count_wave <= (others => '0');
        tl_out <= '0';
    elsif(rising_edge(clk)) then 
        case state is
            when init =>
                if (slope_in = '0') and (previous_slope = '1') then
                count_peaks <= count_peaks +1;
                end if;
                if count_peaks = count_peaks_size +1 then 
                    state <= counting;
                    end if;
            when counting =>
                counter_ready <= '0';
                tl_out <= '0';
                counter <= counter + 1;
                if counter = 201 then 
                    state <= too_long;
                elsif counter = 1000 then 
                    --counter <= (others => '1');
                    state <= show;
                    elsif(slope_in = '0') and (previous_slope = '1')  then
                        count_wave <= count_wave +1;
                    end if;
                if count_wave = 2 then
                    state <= show;
                end if;
            when show =>
                state <= counting;
                counter_out <= std_logic_vector(counter);   
                counter_ready <= '1';
                counter <= to_unsigned(1,counter'length);       -- counter_size
                count_wave <= (others => '0');
            when too_long =>
                tl_out <= '1';
                state <= counting;
                counter_out <= std_logic_vector(counter); --std_logic_vector(tl_mult(counter_size - 1 downto 0));   
                counter_ready <= '1';
                --counter <= to_unsigned(1,counter'length);
                --count_wave <= (others => '0');
                
                
                
        end case;
    end if;
end process;

end Behavioral;
