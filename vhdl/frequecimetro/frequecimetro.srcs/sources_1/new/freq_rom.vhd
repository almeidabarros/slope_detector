library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.frequencimetro_pkg.all;

use IEEE.NUMERIC_STD.ALL;


entity freq_rom is
    Port ( freq_in : in STD_LOGIC_VECTOR (counter_size -1 downto 0);
           bpm : out STD_LOGIC_VECTOR (freq_size -1 downto 0));
end freq_rom;

architecture Behavioral of freq_rom is


begin


bpm <= rom(to_integer(unsigned(freq_in)));


end Behavioral;
