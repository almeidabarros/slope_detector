library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.frequencimetro_pkg.all;

-- frequencia de clk = 100 Hz => 100 amostras por segundo
-- amostras para indicar mudan�a de slope = 10
-- tempo para indicar mudanca de slope = 10 / 100 = 0.1


entity slope_detector is
    Port ( data_in : in STD_LOGIC_VECTOR (data_size -1 downto 0);
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           slope : out STD_LOGIC);
end slope_detector;

architecture Behavioral of slope_detector is

signal s0,s1 : UNSIGNED (data_size -1 downto 0);

--signal counter : signed(4 downto 0);
signal counter : integer range -trend_slope to trend_slope;


type t_state is (INIT,RISING,FALLING);

signal c_state,n_state : t_state;



begin

FSM_CS_PROC: process(clk,rst)
begin

    if rst = '1' then
        c_state <= init;
    
    elsif rising_edge(clk) then
        c_state <= n_state;
    end if;
end process;

count_and_dif : process(clk)
begin
    
    if rising_edge(clk) then
    case c_state is
        when init =>
            counter <= 0; --(others => '0');
            s1 <= (others => '0');
            s0 <= (others => '0');
            
        when others =>
            s0 <= unsigned(data_in);
            s1 <= s0;
            
            if (s1 > s0) and (counter > -trend_slope) then
                counter <= counter - 1;
            elsif (s1 <s0)  and (counter < trend_slope) then
                counter <= counter + 1;
            end if;
    end case;
    end if;
end process;

FSM_NS_PROC: process(c_state,counter)
    begin
    
    case c_state is
        when INIT =>
            n_state <= falling;
        when rising =>
            if counter = -trend_slope then
                n_state <= falling;
            end if;
        when falling =>
            if counter = trend_slope then
                n_state <= rising;              
            end if;
        when others =>
            null;
    end case;
end process;

FSM_OUT_PROC: process(clk,rst)
begin 
    if(rising_edge(clk)) then
    case c_state is 
        
        when init =>
            slope <= '0';
            
        when rising =>
            slope <= '1';

        when falling =>
            slope <= '0';
            

    end case;
    end if;
end process;




end Behavioral;
