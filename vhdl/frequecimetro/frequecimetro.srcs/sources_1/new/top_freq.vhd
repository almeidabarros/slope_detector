library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.frequencimetro_pkg.all;

entity top_freq is
    Port ( 
           --reset em nivel alto
           rst : in STD_LOGIC;
           --clock
           clk : in STD_LOGIC;
           --dados possivelmente provenientes de um conversor analógico digital
           data_in : in STD_LOGIC_VECTOR (data_size -1 downto 0);
           --media movel da frequencia cardiaca em bpm
           freq_out : out STD_LOGIC_VECTOR (freq_size -1 downto 0);
           --frequencia cardiaca instantanea
           freq_inst : out STD_LOGIC_VECTOR (freq_size -1 downto 0);
           --flag que indica que a frquencia pode ser lida
           freq_ready : out STD_LOGIC);
end top_freq;

architecture Behavioral of top_freq is

--estados
type t_state is (init,shift,append,add,div_bpm,mult_bpm);
-- sinal de estados
signal state : t_state;    
-- soma da media movel
signal sum : unsigned(sum_avg_size -1 downto 0);
-- inclinacao
signal s_slope : STD_LOGIC;
 -- frequencia pronta
 signal s_freq_ready : STD_LOGIC;
 -- contagem do contador 
 signal s_freq : STD_LOGIC_VECTOR (counter_size -1 downto 0);
 -- contagem do contador 
signal s_counter_div : STD_LOGIC_VECTOR (counter_size -1 downto 0);
--frquencia cardiaca vinda da rom
signal s_bpm_rom : STD_LOGIC_VECTOR (freq_size -1 downto 0);
-- divisão para calcular bpm
signal s_bpm_div : unsigned(9 downto 0);
-- multiplicação para calcular bpm
signal s_bpm_mult : unsigned(12 downto 0);
--divisão para calcular bpm instantanea
signal s_bpm_div_inst : unsigned(9 downto 0);
-- multiplicação para calcular bpm instantanea
signal s_bpm_mult_inst : unsigned(12 downto 0);
-- flag que indica que a frquencia demorou mais que o tempo maximo para ser atualizada (tl = too long)
signal s_tl_in : std_logic;
-- array da media movel  
type t_avg is array (0 to avg_size -1) of unsigned(counter_size -1 downto 0);
signal avg : t_avg;
--numeros de elementos na array
signal counter :  integer range 0 to avg_size;


begin

dut_slope : slope_detector
    port map(
           data_in  => data_in,
           clk      => clk,
           rst      =>  rst,
           slope    => s_slope );

     dut_freq : freq
     port map ( 
           clk          => clk,
           rst          => rst,
           slope_in     => s_slope,
           tl_out       => s_tl_in,
           counter_out     => s_freq,
           counter_ready   => s_freq_ready );
   
      dut_rom  : freq_rom 
    port map ( 
           freq_in  =>  std_logic_vector(sum(11 downto 2)),
           bpm      =>  s_bpm_rom);      

   ns : process(clk,rst)
   begin
   if rst = '1' then 
    counter <= 0; --(others => '0');
    avg <= (others => (others => '1'));
    sum <= (others => '1');
    state <= init;
    
    elsif(rising_edge(clk)) then 
    freq_out <= std_logic_vector(s_bpm_mult(freq_size -1 downto 0));
    freq_inst <= std_logic_vector(s_bpm_mult_inst(freq_size -1 downto 0));
    
    
    case state is 
        when init => 
            state <= append;
            freq_ready <= '0';
        when append =>
            freq_ready <= '0';
            if s_freq_ready = '1' then
            avg(0) <= unsigned(s_freq);
            if counter < avg_size then 
            counter <= counter + 1;
            end if;
            if s_tl_in = '0' then 
                state <= shift;
                s_counter_div <= s_freq;
            else
                state <= add;
                if s_freq > s_counter_div then
                    s_counter_div <= s_freq;
                end if;
            end if;
            else 
                
            end if;
        when shift =>
            avg <= avg(1 to avg_size -1) & avg(0);
            if counter < avg_size then 
                state <= append;
            else 
                state <= add;
            end if;
        when add =>
            sum <= resize(avg(0),sum'length) + resize(avg(1),sum'length) + resize(avg(2),sum'length) + resize(avg(3),sum'length);
            state <= div_bpm;
        when div_bpm =>
            
            s_bpm_div <= (to_unsigned(1000,10) / sum(11 downto 2));
            s_bpm_div_inst <= to_unsigned(1000,10) / unsigned(s_counter_div);
            state <=  mult_bpm;
        when mult_bpm =>
            s_bpm_mult <= s_bpm_div * time_mult;
            s_bpm_mult_inst <= s_bpm_div_inst * time_mult;
            state <= append;
            freq_ready <= '1';
            
    end case;
    end if;
   end process;
   
   

end Behavioral;
